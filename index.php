<?php
	$data = '1984-08-07';
	function sklonen($n,$s1,$s2,$s3, $b = false){
		$m = $n % 10; $j = $n % 100;
		if($b) $n = '<b>'.$n.'</b>';
		if($m==0 || $m>=5 || ($j>=10 && $j<=20)) return $n.' '.$s3;
		if($m>=2 && $m<=4) return  $n.' '.$s2;
		return $n.' '.$s1;
	}
	
	function znakZodiaka($data){
	$day = str_replace("-","",substr($data,5));
	$zodiak = array('ot' => array('0120','0219','0321','0421','0521','0622','0723','0823','0923','1024','1123','1222','0101'),
		'do' => array('0218','0320','0420','0520','0621','0722','0822','0922','1023','1122','1221','1231','0119'),
		'zn' => array(
			'&#9810; Водолей',
			'&#9811; Рыбы',
			'&#9800; Овен',
			'&#9801; Телец',
			'&#9802; Близнец',
			'&#9803; Рак',
			'&#9804; Лев',
			'&#9805; Дева',
			'&#9806; Весы',
			'&#9807; Скорпион',
			'&#9808; Стрелец',
			'&#9809; Козерог',
			'&#9809; Козерог'));
		$i = 0;
		while (empty($znak) && ($i < 13)){
			$znak = (($zodiak['ot'][$i] <= $day) && ($zodiak['do'][$i] >= $day)) ? $zodiak['zn'][$i] : null;
			++$i;
		} return $znak;
	}

	function calculate_age($data) {
	  $birthday_timestamp = strtotime($data);
	  $age = date('Y') - date('Y', $birthday_timestamp);
	  if (date('md', $birthday_timestamp) > date('md')) {
		$age--;
	  }
	  return $age;
	}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
	<title>Определение знака зодиака</title>
	<style>
		<!--
		body, html {
			margin:0;
			padding:0;
			width:100%;
			font-family:Tahoma;
		}
		.znak:first-letter{
			color: #12e21a;
			font-size: 20px;
		}
		small {
			color:#999;
		}
		-->
	</style>
</head>
<body>
<br/>
<br/>
<br/>
<center>
<?php
	echo '<div class="znak">'.znakZodiaka($data).' | <small>' . sklonen( calculate_age($data), 'год', 'года', 'лет') . '</small></div><br/>';
?>
</center>

<body>
</html>